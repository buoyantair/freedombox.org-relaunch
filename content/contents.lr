title:
---
body:

<section id="freedombox" class="darken">
	<img class="logo" src="images/logo-big.png" alt="FreedomBox">
	<h1>The box running your services<br> from your home.</h1>
	<p class="icon">FreedomBox is a private server for non-experts: install & configure apps with a few clicks. It runs on cheap hardware of your choice, uses your internet connection and power, and is under your control.</p>
	<p><img src="images/screenshot.jpg" class="screenshot" alt="Screenshot"></p>
	<p><a href="#get" class="button">get your FreedomBox</a> <a href="https://discuss.freedombox.org/t/try-a-freedombox-demo/98">try a demo</a></p>
</section>

<section id="privacy">
	<h1>Because your privacy<br> belongs to you.</h1>
	<p class="icon">Your digital life should not be in the hands of tech companies or governments. Keep it close to you. Literally.</p>
	<p><em>We need to de-virtualize the servers where your life is stored, and we need to restore some autonomy to you as the owner of the server.<span>Eben Moglen (2010)</span></em></p>
</section>

<section id="trust" class="darken">
	<h1>You can trust us. <br> Because we value user freedom.</h1>
	<p class="icon">FreedomBox is <a href="https://www.gnu.org/philosophy/free-sw.en.html">Free Software</a> an official part of <a href="https://www.debian.org/">Debian</a>, a well established GNU/Linux distribution. We are backed by
the non-profit <a href="https://freedomboxfoundation.org/">FreedomBox Foundation</a>.</p>
	<p><a href="https://salsa.debian.org/freedombox-team/plinth" class="button">see our source code</a></p>
</section>

<section id="pioneer">
	<h1>Be a Pioneer.</h1>
	<p>FreedomBox is still in its early stages and sometimes requires willingness to leave your comfort zone to deal with rough edges. Give our supported apps a try:</p>
	<ul>
		<li class="radicale">Radicale</li>
		<li class="ejabberd">ejabberd</li>
		<li class="syncthing">Syncthing</li>
		<li class="tor">TOR</li>
		<li class="mumble">Mumble</li>
		<li class="minetest">Minetest</li>
		<li class="tinytinyrss">TinyTinyRSS</li>
		<li class="dots">&nbsp</li>
	</ul>
	<p><a href="https://wiki.debian.org/FreedomBox/Features" class="button">and many more…</a></p>
</section>

<section id="community" class="darken">
	<h1>Get in touch with the community.</h1>
	<p>We are part of the greater Free Software community and welcome everybody. Please get in touch with us if you want join our effort:</p>
	<p><a href="https://discuss.freedombox.org" class="button">get support</a></p>
	<p><a href="https://wiki.debian.org/FreedomBox/Support" class="button">join our chatroom</a></p>
	<p><a href="https://salsa.debian.org/freedombox-team/plinth/issues" class="button">report issues</a></p>
</section>

<section id="get">
	<h1>Buy a pre-built FreedomBox!</h1>
	<p><img src="images/box.jpg" alt="FreedomBox hardware"><a href="https://www.olimex.com/Products/OLinuXino/Home-Server/Pioneer-FreedomBox-HSK/" class="button">buy our "Pioneer Edition" (82€)</a></p>
	<p><em>OR:</em><span>download & install for your device</span></p>
	<ul id="download">
		<li><a href="images/lime2" class="lime2">A20 OLinuXino LIME 2</a></li>
		<li><a href="images/lime" class="lime">A20 OLinuXino LIME</a></li>
		<li><a href="images/micro" class="micro">A20 OLinuXino MICRO</a></li>
		<li><a href="images/beaglebone-black" class="beaglebone-black">BeagleBone Black</a></li>
		<li><a href="images/raspberry-pi-2" class="raspberry-pi-2">Raspberry Pi 2</a></li>
		<li><a href="images/raspberry-pi-3" class="raspberry-pi-3">Raspberry Pi 3</a></li>
		<li><a href="images/cubieboard2" class="cubieboard2">CubieBoard 2</a></li>
		<li><a href="images/cubieboard3" class="cubieboard3">CubieBoard 3</a></li>
		<li><a href="images/pcduino3" class="pcduino3">pcDuino 3</a></li>
		<li><a href="images/qemu64bit" class="qemu64bit">Qemu 64bit</a></li>
		<li><a href="images/qemu32bit" class="qemu32bit">Qemu 32bit</a></li>
		<li><a href="images/virtualbox64bit" class="virtualbox64bit">VirtualBox 64bit</a></li>
		<li><a href="images/virtualbox32bit" class="virtualbox32bit">VirtualBox 32bit</a></li>
		<li><a href="images/x86-64bit" class="x86-64bit">64-bit x86 (amd64)</a></li>
		<li><a href="images/x86-32bit" class="x86-32bit">32-bit x86 (i386)</a></li>
		<li><a href="https://discuss.freedombox.org/t/install-freedombox-on-debian/93" class="debian">Debian installer</a></li>
		<li><a href="https://discuss.freedombox.org/t/install-freedombox-on-amazon-aws/97" class="aws">AWS installer</a></li>
	</ul>
</section>
